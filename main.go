package main

import (
	"fmt"
	"log"
	"math"

	"github.com/hybridgroup/gobot"
	"github.com/hybridgroup/gobot/platforms/leap"
)

const (
	movementDelta = 0.5
)

var (
	messageChan = make(chan string)
)

func calcDiffHands(hand, oHand leap.Hand) (float64, float64, float64) {
	return hand.X() - oHand.X(), hand.Y() - oHand.Y(), hand.Z() - oHand.Z()
}

func messager() {
	for msg := range messageChan {
		log.Print(msg)
	}
}

func isMovement(diff float64) bool {
	return math.Abs(diff) > movementDelta
}

func processHands(handsChan chan []leap.Hand) {
	oldHands := make(map[int]leap.Hand)

	for hands := range handsChan {
		for i, hand := range hands {
			if oldHand, ok := oldHands[i]; ok {
				x, y, z := calcDiffHands(hand, oldHand)
				if isMovement(x) ||
					isMovement(y) ||
					isMovement(z) {
					messageChan <- fmt.Sprintf("Hand(%d): X:%f Y:%f Z:%f", i, x, y, z)
				}
			}
			oldHands[i] = hand
		}
	}
}

func main() {
	gbot := gobot.NewGobot()

	leapMotionAdaptor := leap.NewLeapMotionAdaptor("leap", "127.0.0.1:6437")
	l := leap.NewLeapMotionDriver(leapMotionAdaptor, "leap")

	handsChan := make(chan []leap.Hand)

	go messager()
	go processHands(handsChan)

	work := func() {
		gobot.On(l.Event("message"), func(data interface{}) {
			frm, ok := data.(leap.Frame)
			if !ok {
				log.Printf("Error type cast")
				return
			}
			handsChan <- frm.Hands
		})
	}

	robot := gobot.NewRobot("leapBot",
		[]gobot.Connection{leapMotionAdaptor},
		[]gobot.Device{l},
		work,
	)
	gbot.AddRobot(robot)
	gbot.Start()
}
